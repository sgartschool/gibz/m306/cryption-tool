function methodChanged(){
    let methodValue = document.getElementById("method").value;
    let modeSelect = document.getElementById("mode");
    if (methodValue === "1" || methodValue === "2"){
        // ToDo: Lock select on "Encrypt"
    }
    else {
        // ToDo: Unlock select
    }
}

function showError(message){
    M.toast({
        html: message,
        classes: 'aurora-red'
    });
}

function formSubmit(){
    let form = document.getElementById('input-form');
    if (checkInput(form)){
        convert(form);
    }
}

function checkInput(form){
    let valid = true;

    if (!form.source.value.match(/[ -~]/)){
        valid = false;
        document.getElementById("source").classList.add("invalid");
        showError('Please enter a valid source! Valid are all ascii characters between 32 and 128.');
        try {
            umami.trackEvent('Invalid source', 'error');
        }
        finally {
            // Do nothing
        }
    }
    else
        document.getElementById("source").classList.remove("invalid");

    if (form.method.value === '0'){
        valid = false;
        showError('Please select a method!');
        try {
            umami.trackEvent('No method chosen', 'error');
        }
        finally {
            // Do nothing
        }
    }

    if (form.mode.value === '0'){
        valid = false;
        showError('Please select a mode!');
        try{
            umami.trackEvent('No mode chosen', 'error');
        }
        finally {
            // Do nothing
        }
    }

    if (!form.salt.value.match(/([ -~])?/)){
        valid = false;
        document.getElementById("salt").classList.add("invalid");
        showError('Please enter a valid SALT! Valid are all ascii characters between 32 and 128.');
        try {
            umami.trackEvent('Invalid SALT', 'error');
        }
        finally {
            // Do nothing
        }
    }
    else if (form.salt.value === "" && form.method.value === "6"){
        valid = false;
        document.getElementById("salt").classList.add("invalid");
        showError('Method needs a SALT!');
        try {
            umami.trackEvent('Method needs a SALT', 'error');
        }
        finally {
            // Do nothing
        }
    }
    else if (form.method.value === "3"){
        if (form.salt.value === "" || parseInt(form.salt.value) > 26 || parseInt(form.salt.value) < 0){
            valid = false;
            document.getElementById("salt").classList.add("invalid");
            showError('Enter shift amount in SALT field! Shift can\'t be higher than 26 and lower than 0');
            try{
                umami.trackEvent('Invalid shift amount', 'error');
            }
            finally {
                // Do nothing
            }
        }
    }
    else
        document.getElementById("salt").classList.remove("invalid");

    return valid;
}

function convert(form){
    let source = form.source.value;
    let method = form.method.value;
    let mode = form.mode.value;
    let salt = form.salt.value;
    let output = form.output;
    let result = "Converting...";
    let error = false;

    output.value = result;

    switch(mode){
        case '1':
            switch(method){
                case '1':
                    result = encryptMd5(source);
                    try {
                        umami.trackEvent(
                            'md5 encryption',
                            'conversion'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                case '2':
                    result = encryptSha256(source);
                    try {
                        umami.trackEvent(
                            'sha256 encryption',
                            'conversion'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                case '3':
                    result = encryptCaesar(source, parseInt(salt));
                    try {
                        umami.trackEvent(
                            'Caesar encryption',
                            'conversion'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                case '4':
                    result = encryptMorse(source);
                    try {
                        umami.trackEvent(
                            'Morse code encryption',
                            'conversion'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                case '5':
                    result = encryptBase64(source);
                    try {
                        umami.trackEvent(
                            'base64 encryption',
                            'conversion'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                case '6':
                    result = encryptVigniere(source, salt);
                    try {
                        umami.trackEvent(
                            'Vigniere encryption',
                            'conversion'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                default:
                    showError('Unknown method!');
                    error = true;
            }
            break;
        case '2':
            switch(method){
                case '1':
                    showError('Can\'t decrypt md5 hash!');
                    error = true;
                    try {
                        umami.trackEvent(
                            'md5 decryption',
                            'error'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                case '2':
                    showError('Can\'t decrypt sha256 hash!');
                    error = true;
                    try {
                        umami.trackEvent(
                            'sha256 decryption',
                            'error'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                case '3':
                    result = decryptCaesar(source, parseInt(salt));
                    try {
                        umami.trackEvent(
                            'Caesar decryption',
                            'conversion'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                case '4':
                    result = decryptMorse(source);
                    try {
                        umami.trackEvent(
                            'Morse code decryption',
                            'conversion'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                case '5':
                    result = decryptBase64(source);
                    try {
                        umami.trackEvent(
                            'base64 decryption',
                            'conversion'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                case '6':
                    result = decryptVigniere(source, salt);
                    try {
                        umami.trackEvent(
                            'Vigniere decryption',
                            'conversion'
                        );
                    }
                    finally {
                        // Do nothing
                    }
                    break;
                default:
                    showError('Unknown method!');
                    error = true;
                    try {
                        umami.trackEvent(
                            'Unknown method',
                            'error'
                        );
                    }
                finally {
                    // Do nothing
                }
            }
            break;
        default:
            showError('Unknown mode!');
            error = true;
            try {
                umami.trackEvent(
                    'Unknown mode',
                    'error'
                );
            }
            finally {
                // Do nothing
            }
    }

    if (error)
        output.value = 'Something went wrong while converting!';
    else
        output.value = result;
}