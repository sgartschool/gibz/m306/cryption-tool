function decryptMorse(source){
    const MORSE_CODE = {
        ".-": "a",
        "-...":"b",
        "-.-.": "c",
        "-..": "d",
        ".":"e",
        "..-.":"f",
        "--.":"g",
        "....":"h",
        "..":"i",
        ".---":"j",
        "-.-":"k",
        ".-..":"l",
        "--":"m",
        "-.":"n",
        "---":"o",
        ".--.":"p",
        "--.-":"q",
        ".-.":"r",
        "...":"s",
        "-":"t",
        "..-":"u",
        "...-":"v",
        ".--":"w",
        "-..-":"x",
        "-.--":"y",
        "--..":"z",
        ".----":"1",
        "..---":"2",
        "...--":"3",
        "....-":"4",
        ".....":"5",
        "-....":"6",
        "--...":"7",
        "---..":"8",
        "----.":"9",
        "-----":"0",
        "|":" ",
        "._...":"&",
        ".----.":"'",
        ".--.-.":"@",
        "-.--.-":")",
        "-.--.":"(",
        "---...":":",
        "--..--":",",
        "-...-":"=",
        "-.-.--":"!",
        ".-.-.-":".",
        "-....-":"-",
        "----- -..-. -----":"%",
        ".-..-.":"\"",
        "..--..":"?",
        "-..-.":"/"
    };
    return source.toLowerCase().split(" ").map(el => {
        return MORSE_CODE[el] ? MORSE_CODE[el] : el;
    }).join("");
}

function decryptBase64(source){
    return atob(source);
}

function decryptVigniere(source, key){
    let result = ''

    for (let i = 0, j = 0; i < source.length; i++) {
        const c = source.charAt(i)
        if (isLetter(c)) {
            if (isUpperCase(c)) {
                result += String.fromCharCode(90 - (25 - (c.charCodeAt(0) - key.toUpperCase().charCodeAt(j))) % 26)
            } else {
                result += String.fromCharCode(122 - (25 - (c.charCodeAt(0) - key.toLowerCase().charCodeAt(j))) % 26)
            }
        } else {
            result += c
        }
        j = ++j % key.length
    }
    return result
}

function decryptCaesar(text, shift) {
    shift = (26 - shift) % 26;
    let UPPER_A = "A".charCodeAt(0);
    let LOWER_A = "a".charCodeAt(0);
    let result = "";
    for (let i = 0; i < text.length; i++) {
        let c = text.charCodeAt(i);
        if (UPPER_A <= c && c <= "Z".charCodeAt(0))  // Uppercase
            c = (c - UPPER_A + shift) % 26 + UPPER_A;
        else if (LOWER_A <= c && c <= "z".charCodeAt(0))  // Lowercase
            c = (c - LOWER_A + shift) % 26 + LOWER_A;
        result += String.fromCharCode(c);
    }
    return result;
}