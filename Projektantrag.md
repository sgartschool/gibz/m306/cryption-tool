|Projektantrag||
|:--------:|-------|
|Projektname|Online Cryption Tool|
|Projektnummer|26|
|Ausgangslage|Es gibt viele einzelne Cryption-Tools im Internet. Nun sollen diese einzelnen Tools in einem zusammengefasst werden.|
|Projektziele|Mit diesem Projekt sollen folgende Ziele erreicht werden: <br> - Es soll mit 5 Verschlüsselungsarten sowie 5 Hashingmethoden Text verschlüsselt werden können.<br /> - Es soll mit denselben 5 Entschlüsselungsarten Text entschlüsselt werden können.<br /> - Die Verschlüsselung und Entschlüsselung soll serverseitig ablaufen.|
|Abgrenzungen|Keine übergrossen Features wie zB. automatische Auswahl der richtigen Entschlüsselung für Text|
|Meilensteine|- Dokumentation<br> - Verschlüsselungsarten suchen und definieren (Entschlüsselung / Verschlüsselung / Beides)<br> - Verschlüsselung von Text mit ausgewählter Verschlüsselung<br> - Entschlüsselung von Text mit ausgewählter Entschlüsselung|
|Termine|- Projektstart: 2021-03-20<br> - Meilenstein 1 Start: 2021-03-30<br> - Meilenstein 1 Ende: 2021-04-06<br> - Meilenstein 2 Start: 2021-04-06<br> - Meilenstein 3 Start: 2021-04-06<br> - Projektende: 2021-05-25|
|Kosten|Das Projekt ist intern, somit fallen keine zusätzlichen Kosten an|
|Personenaufwand|3 Personen|
|Sachmittel|- Internet<br> - Computer<br> - IDE|
|Auftraggeber|Mauro|
|Projektleiter|Samuel|
|Projektmitglieder|Philipp N., Mauro|
|Projektausschuss|-|
|Verteiler|- Auftraggeber<br> - Projektleiter<br> - Projektmitglieder|
|Unterschrift|-|
|Beilagen|-|
